<!DOCTYPE html>
<html>
<head>
    <title>Form Sign Up</title>
</head>
<body>

<h1>Buat Account Baru!</h1>

<h2>Sign Up Form</h2>
<form action="/welcome" method="post">
    @csrf
    <p>First name:</p>
    <input type="text" name="firstname" id="firstname" required>
    
    <p>Last name:</p>
    <input type="text" name="lastname" id="lastname" required>

    <p>Gender:</p>
    <input type="radio" id="male" name="gender" value="male" required>
    <label for="male">Male</label><br>
    <input type="radio" id="female" name="gender" value="female">
    <label for="female">Female</label><br>
    <input type="radio" id="other" name="gender" value="other">
    <label for="other">Other</label>

    <p>Nationality:</p>
    <select name="nation" id="nation">
        <option value="indonesia">Indonesian</option>
        <option value="malaysia">Malaysian</option>
        <option value="singapore">Singaporean</option>
        <option value="vietnam">Vietnamese</option>
    </select>

    <p>Language Spoken:</p>
    <input type="checkbox" id="indonesia" name="indonesia" value="Bahasa Indonesia">
    <label for="indonesia">Bahasa Indonesia</label><br>
    <input type="checkbox" id="english" name="english" value="English">
    <label for="vehicle2">English</label><br>
    <input type="checkbox" id="other" name="other" value="Other">
    <label for="vehicle3">Other</label><br>

    <p>Bio:</p>
    <textarea id="bio" name="bio" rows="10" cols="30" required></textarea><br>
    <input type="submit" value="Sign Up">
</form>

</body>
</html>