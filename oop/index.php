<?php
require ('Frog.php');

// release 0
$sheep = new Animal("shaun");

echo $sheep->name . "<br>";
echo $sheep->legs . "<br>";
echo $sheep->cold_blooded . "<br>";
echo "<br>";

// release 1
$sungokong = new Ape("kera sakti");
echo $sungokong->name . "<br>";
echo $sungokong-> legs . "<br>";
echo $sungokong->yell() ."<br>";
echo "<br>";

$kodok = new Frog("buduk");
echo $kodok->name . "<br>";
echo $kodok-> legs . "<br>";
echo $kodok->jump();
?>